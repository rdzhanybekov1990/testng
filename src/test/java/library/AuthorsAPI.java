package library;

import pojos.Authors;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class AuthorsAPI {
    private static String uri = "http://fakerestapi.azurewebsites.net";
    private static Logger logger = LoggerFactory.getLogger(AuthorsAPI.class);

    public static Response getAuthor() {
        RestAssured.baseURI = uri;
        RequestSpecification request = RestAssured.given().log().all();
        Response response = request.get(uri + "/api/Authors");
        return response; //list<Map<>
    }

    public static Response getListOfAuthor() {
        RestAssured.baseURI = uri;
        RequestSpecification request = RestAssured.given().log().all();
        Response response = request.get(uri + "/api/Authors");
        return response;
    }

    public static Response getAuthorByID(int ID) {
        RestAssured.baseURI = uri;
        RequestSpecification request = RestAssured.given().log().all();
        request.headers("Content-Type", "application/json; charset=utf-8");
        Response response = request.get(uri + "/api/Authors/" + ID);
        response.body().prettyPrint();
        return response;
    }

    public static Authors getAuthorByIDtoPojo(int ID) throws IOException {

        RestAssured.baseURI = uri;
        RequestSpecification request = RestAssured.given().log().all();
        request.headers("Content-Type", "application/json; charset=utf-8");
        Response response = request.get(uri + "/api/Authors/" + ID);
        String json = response.getBody().asString();
        response.body().prettyPrint();
        ObjectMapper mapper = new ObjectMapper();
        Authors author = mapper.readValue(json, Authors.class);
        return author;
    }

    public static Authors getListOfAuthors(int ID) throws IOException {
        RestAssured.baseURI = uri;
        RequestSpecification request = RestAssured.given().log().all();
        Response response = request.get(uri + "/api/Authors");
        String json = response.getBody().asString();
        ObjectMapper mapper = new ObjectMapper();
        Authors myObjects = mapper.readValue(json, new TypeReference<List<Authors>>() {});
        return myObjects;
    }
}
