package library;

import pojos.Books;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import utils.Utilities;

import java.io.IOException;
import java.util.List;

public class BooksAPI {

    private static String uri = "http://fakerestapi.azurewebsites.net";
    private static Logger logger = LoggerFactory.getLogger(BooksAPI.class);

    public static List<Books> getBooks() throws IOException {
        RestAssured.baseURI = uri;
        RequestSpecification request = RestAssured.given().log().all();
        request.headers("Accept", "application/json");
        Response response = request.get(uri + "/api/Books");
        String json = response.getBody().asString();
        Assert.assertEquals(response.getStatusCode(), 200);
        ObjectMapper mapper = new ObjectMapper();
        logger.info("Response is: " + response.getBody().prettyPrint());
        return mapper.readValue(json, new TypeReference<List<Books>>() {
        });
    }

    public static Books getBookByID(int bookId) throws IOException {
        RestAssured.baseURI = uri;
        RequestSpecification request = RestAssured.given().log().all();
        Response response = request.get(uri + "/api/Books/" + bookId);
        String json = response.getBody().asString();
        Assert.assertEquals(response.getStatusCode(), 200);
        ObjectMapper mapper = new ObjectMapper();
        Books book = mapper.readValue(json, Books.class);
        return book;
    }

    public static Books updateBooks(Books book, int ID) throws IOException {

        //SERIALIZATION FROM OBJECT TO JSON
        ObjectMapper mapper = new ObjectMapper();
        book.Title = Utilities.properties.getProperty("title");
        book.Description = "New Description";
        book.PageCount = 1000;
        String jsonRequest = mapper.writeValueAsString(book);

        //BUILDING REQUEST:
        RestAssured.baseURI = uri;
        RequestSpecification request = RestAssured.given().log().all();
        request.headers("Content-Type", "application/json");

        request.body(jsonRequest);

        //CREATE RESPONSE
        Response response = request.put(uri + "/api/Books/" + ID);
        String jsonResponse = response.getBody().asString();

        //DESERIALIZE THE RESPONSE
        Books bookUpdated = mapper.readValue(jsonResponse, Books.class);
        return bookUpdated;
    }
}
