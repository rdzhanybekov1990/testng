package library;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import pojos.Clients;

import java.io.IOException;
import java.util.List;

public class ClientsAPI {

    private static String uri = "https://jsonplaceholder.typicode.com";
    private static Logger logger = LoggerFactory.getLogger(Clients.class);

    public static List<Clients> getClients() throws IOException {
        RestAssured.baseURI = uri;
        RequestSpecification request = RestAssured.given().log().all();
        request.headers("Accept", "application/json");
        Response response = request.get(uri + "/users");
        String json = response.getBody().asString();
        Assert.assertEquals(response.getStatusCode(), 200);
        ObjectMapper mapper = new ObjectMapper();
        logger.info("Response is: " + response.getBody().prettyPrint());
        return mapper.readValue(json, new TypeReference<List<Clients>>() {});
    }
}
