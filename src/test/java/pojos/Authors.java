package pojos;

public class Authors {
    private int ID;
    private int IDBook;
    private String FirstName;
    private String LastName;


    public int getID() {
        return ID;
    }

    public int getIDBook() {
        return IDBook;
    }

    public String getFirstName() {
        return FirstName;
    }

    public String getLastName() {
        return LastName;
    }


    @Override
    public String toString() {
        return "Authors{" +
                "ID=" + ID +
                ", IDBook=" + IDBook +
                ", FirstName='" + FirstName + '\'' +
                ", LastName='" + LastName + '\'' +
                '}';
    }
}
