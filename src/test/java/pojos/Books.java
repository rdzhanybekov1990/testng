package pojos;

public class Books {

    public Integer ID;
    public String Title;
    public String Description;
    public Integer PageCount;
    public String Excerpt;
    public String PublishDate;


    @Override
    public String toString() {
        return "Books{" +
                "ID=" + ID +
                ", Title='" + Title + '\'' +
                ", Description='" + Description + '\'' +
                ", PageCount=" + PageCount +
                ", Excerpt='" + Excerpt + '\'' +
                ", PublishDate='" + PublishDate + '\'' +
                '}';
    }

}
