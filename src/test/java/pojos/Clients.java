package pojos;

public class Clients {

    public int id;
    public String name;
    public String username;
    public String email;
    public Address address;
    public String phone;
    public String website;
    public Company company;

    public class Address {
        public String street;
        public String suite;
        public String city;
        public String zipcode;
        public Geo geo;
    }

    public static class Geo {
        public String lat;
        public String lng;
    }

    public static class Company {
        public String name;
        public String catchPhrase;
        public String bs;
    }

    @Override
    public String toString() {
        return "Clients{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", username='" + username + '\'' +
                ", email='" + email + '\'' +
                ", address=" + address +
                ", phone='" + phone + '\'' +
                ", website='" + website + '\'' +
                ", company=" + company +
                '}';
    }

/*
        1. Create pojo "Customers" for "https://jsonplaceholder.typicode.com/users"
        2. Create a method inside CustomersAPI
        3. Create a test getCustomers;
        4. Assert size of the List is 10
        5. Assert in List of Customers: customers.get(0).company.name equal to "Romaguera-Crona"
        6. Assert that first customers get latitude is ""-37.3159"
         */


}