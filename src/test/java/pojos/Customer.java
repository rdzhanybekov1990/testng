package pojos;

public class Customer {
    public double Id;
    public String Name;
    public String Address;
    public int Age;

    @Override
    public String toString() {
        return "Customer{" +
                "Id=" + Id +
                ", Name='" + Name + '\'' +
                ", Address='" + Address + '\'' +
                ", Age=" + Age +
                "}\n";
    }


}