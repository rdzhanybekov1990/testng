package testng;

import library.AuthorsAPI;
import library.BooksAPI;
import pojos.Authors;
import pojos.Books;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.Assert;
import org.testng.annotations.Test;
import utils.Utilities;

import java.io.IOException;
import java.util.List;

public class BooksTest extends Utilities {

    private static Logger logger = LoggerFactory.getLogger(BooksTest.class);
    private List<Authors> authors;
    private Books books = new Books();
    private Books booksUpdated = new Books();

    @Test
    public void getAuthors() throws IOException {
        authors = AuthorsAPI.getListOfAuthors();
        Assert.assertNotNull(authors);
        System.out.println("Size of the List: " + authors.size());
    }

    @Test
    public void getBooks() throws IOException {
        List<Books> listOfBooks = BooksAPI.getBooks();
        System.out.println("Size of the List: " + listOfBooks.size());
        Assert.assertNotNull(listOfBooks);
    }

    @Test
    public void updateBook() throws IOException {
        books = BooksAPI.getBookByID(4);
        booksUpdated = BooksAPI.updateBooks(books, 4);
        Assert.assertEquals(booksUpdated.ID, Integer.valueOf(4));
        Assert.assertEquals(booksUpdated.Title, "New Title");
        Assert.assertEquals(String.valueOf(booksUpdated.PageCount), "1000");
        Assert.assertEquals(booksUpdated.Description,"New Description");
    }

    @Test
    public void getActivities(){

    }

}





