package testng;

import library.ClientsAPI;
import org.testng.Assert;
import org.testng.annotations.Test;
import pojos.Clients;

import java.io.IOException;
import java.util.List;

public class ClientsTest {

    private String expectedCompanyName = "Romaguera-Crona";
    private String expectedLat = "-37.3159";

    @Test
    public void getClients() throws IOException {
        List<Clients> clients = ClientsAPI.getClients();
        Assert.assertEquals(clients.size(), 10);
        Assert.assertEquals(clients.get(0).company.name, expectedCompanyName);
        Assert.assertEquals(clients.get(0).address.geo.lat, expectedLat);
        System.out.println(clients);
    }
}
