package testng;

import com.fasterxml.jackson.databind.ObjectMapper;
import library.AuthorsAPI;
import org.testng.Assert;
import org.testng.annotations.Test;
import pojos.Authors;
import pojos.Customer;
import utils.DataProviderClass;
import utils.Utilities;

import java.io.IOException;
import java.util.List;

public class CustomerTest {


    @Test
    public void getCustomersFromExcel() throws IOException {
        List<Customer> customers = Utilities.readExcelFileCustomers("Customers.xlsx");
        System.out.println(customers);
    }

    @Test
    public void getExcelToObject() throws IOException {
        Object[][] objects = Utilities.readExcelFileObject("Customers.xlsx", "Products", 4, 5);
        for (int i = 0; i <= objects.length - 1; i++) {
            for (int y = 0; y <= objects[i].length - 1; y++) {
                System.out.print(objects[i][y] + " ");
            }
            System.out.println("");
        }
    }

    @Test(dataProvider = "Customers", dataProviderClass = DataProviderClass.class)
    public void dataProviderMethodTest(String id, String name, String address, String age) {

        System.out.println(id);
        System.out.println(name);
        System.out.println(address);
        System.out.println(age);

    }

    @Test(dataProvider = "Authors", dataProviderClass = DataProviderClass.class)
    public void dataProviderMethodTest(String id, String firstName, String lastName) throws IOException {
       Authors authors= AuthorsAPI.getAuthorByIDtoPojo(Integer.valueOf(id));
        Assert.assertEquals(authors.getFirstName(), firstName);
        Assert.assertEquals(authors.getLastName(), lastName);
    }
}

