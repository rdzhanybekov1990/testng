package testng;

import library.UsersAPI;
import org.testng.Assert;
import org.testng.annotations.Test;
import pojos.Users;

import java.io.IOException;
import java.util.List;

public class UsersTest {

    @Test
    public void getUsers() throws IOException {
        List<Users> listOfUsers = UsersAPI.getUsers();
        Assert.assertNotNull(listOfUsers);
        Assert.assertTrue(listOfUsers.size() > 1);
    }
}
