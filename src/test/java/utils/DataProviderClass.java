package utils;

import org.testng.annotations.DataProvider;

import java.io.IOException;

public class DataProviderClass {

    @DataProvider(name = "Customers")
    public static Object[][] dataProvideCustomers() throws IOException {
        Object[][] objects = Utilities.readExcelFileObject("Customers.xlsx", "Customers", 4, 6);
        return objects;
    }

  //  {{1,jask smith, mass, 23}, {2, Adam jackson}, NY, 27}, {},{}}
}
