package utils;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.annotations.BeforeClass;
import org.testng.xml.dom.OnElement;
import pojos.Customer;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Properties;

public class Utilities {

    public static Properties properties;
    private static Logger logger = LoggerFactory.getLogger(Utilities.class);

    @BeforeClass
    public void setUp() throws IOException {
        properties = new Properties();
        properties.load(new FileInputStream("src/test/resources/config.properties"));
    }

    public static List<List<Object>> readExcelFileToList(String filePath, String sheetName) throws IOException {
        // Object[][]
        List<List<Object>> customers = new ArrayList<>();

        //Create on object of the FileInputStream Class to read excel file
        FileInputStream excelFile = new FileInputStream(new File(filePath));
        //High level representation of a Excel workbook. This is the first object you create to read or write a workbook.
        Workbook workbook = new XSSFWorkbook(excelFile);
        //Represents each sheet in the excel file:
        Sheet sheet = workbook.getSheet(sheetName);
        //Create a Row Iterator so we can store all rows, same as List<String>
        Iterator<Row> rows = sheet.iterator();

        int rowNumber = 0;
        while (rows.hasNext()) {
            Row currentRow = rows.next();

            //Skip the first row - header:
            if (rowNumber == 0) {
                rowNumber++;
                continue;
            }

            Iterator<Cell> cellsInRow = currentRow.iterator();

            List<Object> customer = new ArrayList<>();

            int cellNumber = 0;
            while (cellsInRow.hasNext()) {
                Cell currentCell = cellsInRow.next();

                if (cellNumber == 0) {
                    customer.add(currentCell.getNumericCellValue());
                } else if (cellNumber == 1) {
                    customer.add(currentCell.getStringCellValue());
                } else if (cellNumber == 2) {
                    customer.add(currentCell.getStringCellValue());
                } else if (cellNumber == 3) {
                    customer.add(currentCell.getNumericCellValue());
                }

                cellNumber++;
            }

            customers.add(customer);
        }
        return customers;
    }

    public static List<Customer> readExcelFileCustomers(String filePath) throws IOException {
        List<Customer> customers = new ArrayList<>();
        //Create on object of the FileInputStream Class to read excel file
        FileInputStream excelFile = new FileInputStream(filePath);
        //High level representation of a Excel workbook. This is the first object you create to read or write a workbook.
        Workbook workbook = new XSSFWorkbook(excelFile);
        //Represents each sheet in the excel file:
        Sheet sheet = workbook.getSheet("Customers");
        //Create a Row Iterator so we can store all rows, same as List<String>
        Iterator<Row> rows = sheet.iterator();
        int rowNumber = 0;
        while (rows.hasNext()) {
            Row currentRow = rows.next();
            //Skip the first row - header:
            if (rowNumber == 0) {
                rowNumber++;
                continue;
            }
            Iterator<Cell> cellsInRow = currentRow.iterator();
            Customer customer = new Customer();
            int cellNumber = 0;
            while (cellsInRow.hasNext()) {
                Cell currentCell = cellsInRow.next();
                if (cellNumber == 0) {
                    customer.Id = currentCell.getNumericCellValue();
                } else if (cellNumber == 1) {
                    customer.Name = currentCell.getStringCellValue();
                } else if (cellNumber == 2) {
                    customer.Address = (currentCell.getStringCellValue());
                } else if (cellNumber == 3) {
                    customer.Age = (int) (currentCell.getNumericCellValue());
                }
                cellNumber++;
            }
            customers.add(customer);
        }
        workbook.close();
        return customers;
    }

    public static Object[][] readExcelFileObject(String filePath, String sheetName, int cell, int row) throws IOException {

        Object[][] arrayExcelData = new Object[row][cell];

        FileInputStream fileInputStream = new FileInputStream(filePath);
        Workbook workbook = new XSSFWorkbook(fileInputStream);

        Sheet sheet = workbook.getSheet(sheetName);
        Iterator<Row> rows = sheet.rowIterator();

        int rowNumber = 0;
        int rowIndex = 0;
        while(rows.hasNext()) {
            Row currentRow = rows.next();
            //Skip the headers:
            if(rowNumber == 0) {
                rowNumber ++;
                continue;
            }
            //List<Cell>
            Iterator<Cell> cellsInRow =  currentRow.iterator();
            Object[] cellArray = new Object[cell];
            int cellIndex =0;
            while (cellsInRow.hasNext()) {
                Cell currentCell = cellsInRow.next();
                currentCell.setCellType(Cell.CELL_TYPE_STRING);
                cellArray[cellIndex++] = currentCell.getStringCellValue();
            }
            arrayExcelData[rowIndex] = cellArray;
            rowIndex++;
        }
        workbook.close();
        return arrayExcelData;
    }


    public static String convertToJson(List<List<Object>> customers) throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        String json = "";
        json = mapper.writeValueAsString(customers);
        return json;
    }
}